## Kompilator języka Latte do LLVM
#####Barłomiej Wróblewski, bw371883


#### Kompilacja kompilatora
Kompilacja za pomocą polecenia make
```bash
make
```

#### Użycie kompilatora
Kompilowanie pliku filename.lat w języku Latte do llvm 
```bash
./latc_llvm filename.lat
```
tworzy 2 pliki:
- `filename.ll` - zawierający kod LLVM
- `filename.bc` - plik binarny utworzony na podstawie pliku `filename.ll`

#### Analiza semantyczna
Parsowanie programu jest oparte na BNFC.
Przestrzeń nazw jest wspólna dla zmiennych oraz dla funkcji.
Zabronione jest nadpisywanie wewnątrz funkcji metod bibliotecznych (opis biblioteki póżniej).

Wspierane jest dowolne porównywanie zmiennych liczbowych oraz relacja równości na zmiennych logicznych.

#### Kompilacja
Kompilator obsługuje podstawową wersję języka, bez żadnych rozszerzeń.
Generuje kod LLVM w postaci SSA.

#### Optymalizacje
- Zwijanie i propagacja stałych.
- Usuwanie nieosiągalnych bloków.
- Usuwanie nieosiągalnych instrukcji.
- Usuwanie martwych zmiennych.
- Redukcja mocy.

#### Biblioteka 
Funkcje biblioteczne znajdują się w `lib/runtime.ll`. 
Plik ten został wygenerowany narzędziem clang na podstawie pliku `lib/runtime.c`.

Funkcje biblioteczne w nim zawarte:
```
void printInt(int) - wypisuje zmienną liczbową
void printString(string) - wypisuje zmienną napisową
void error() - wyrzuca błąd wykonania
int readInt() - wczytuje zmienną liczbową oraz omija inne znaki do końca linii
string readString() - wczytuje linię z wejścia, usuwa znak nowej linii
string concat(string, string) - ukryta funckja realizująca konkatenację zmiennych napisowych
```
