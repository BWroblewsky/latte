#!/usr/bin/env bash

set -e
make
set +e

function check_good() {
    dict=$1

    for file in $(ls ${dict}/* | grep .lat); do
        echo ${file}
        ./latc ${file} > /dev/null
        if [[ $? -eq 0 ]]; then
            echo "OK CODE"
        else
            echo "BAD CODE"
            exit 1
        fi

        touch "${dict}/$(basename "${file}" .lat).input"
        lli "${dict}/$(basename "${file}" .lat).bc" < "${dict}/$(basename "${file}" .lat).input" > tmp
        diff tmp "${dict}/$(basename "${file}" .lat).output" >/dev/null

        if [[ $? -eq 0 ]]; then
            echo "OK OUTPUT"
        else
            echo "BAD OUTPUT"
            exit 1
        fi
    done
}

function check_bad() {
    dict=$1

    for file in $(ls ${dict}/* | grep .lat); do
        echo ${file}
        ./latc ${file}
        if [[ $? -eq 1 ]]; then
            echo "OK CODE"
        else
            echo "BAD CODE"
            exit 1
        fi
    done
}

check_good "test/good"
check_good "mrjp-tests/good/basic"

check_bad "test/bad"
check_bad "mrjp-tests/bad/semantic"

rm -rf tmp
