module BackEnd.Common where

import Control.Monad.State

import Data.Char
import Data.Map.Strict as Map
import Data.Maybe
import Data.Set as Set
import Data.List
import Numeric
import System.Exit

import Latte.AbsLatte

data LLVMVar = V String | B Bool | I Integer deriving (Eq, Ord)
data LLVMLab = L String deriving (Eq, Ord)

isConstant :: LLVMVar -> Bool
isConstant (B _) = True
isConstant (I _) = True
isConstant (V _) = False

instance Show LLVMVar where
  show (V s)  = "%" ++ s
  show (B b)  = if b then "1" else "0"
  show (I i)  = show i

instance Show LLVMLab where
  show (L l)  = l

data LLVMType = Int32 | Int8 | Int1 | V0 | Ptr LLVMType deriving (Eq, Ord)

instance Show LLVMType where
  show Int32  = "i32"
  show Int8   = "i8"
  show Int1   = "i1"
  show V0     = "void"
  show (Ptr t) = (show t) ++ "*"

getLLVMType :: Type a -> LLVMType
getLLVMType t = case t of
  (Void _) -> V0
  (Int _) -> Int32
  (Str _) -> Ptr Int8
  (Bool _) -> Int1

data BinOperation = QADD | QSUB | QTIMES | QDIV | QMOD deriving (Eq, Ord)
data CmpOperation = QLTH | QLE | QGTH | QGE | QEQU | QNE deriving (Eq, Ord)
data UnaryOperation = QNOT | QNEG deriving (Eq, Ord)

instance Show CmpOperation where
  show QLTH = "slt"
  show QLE  = "sle"
  show QGTH = "sgt"
  show QGE  = "sge"
  show QEQU = "eq"
  show QNE  = "ne"

data LLVMInstruction = Label LLVMLab
                     | Return LLVMType LLVMVar
                     | ReturnVoid
                     | BinOp LLVMVar BinOperation LLVMVar LLVMVar
                     | CmpOp LLVMVar CmpOperation LLVMType LLVMVar LLVMVar
                     | UnaryOp LLVMVar UnaryOperation LLVMVar
                     | LoadString LLVMVar LLVMLab Int
                     | Call LLVMType LLVMLab [(LLVMType, LLVMVar)]
                     | CallRes LLVMVar LLVMType LLVMLab [(LLVMType, LLVMVar)]
                     | Phi LLVMVar LLVMType [(LLVMVar, LLVMLab)]
                     | Jmp LLVMLab
                     | Br LLVMVar LLVMLab LLVMLab deriving (Eq, Ord)

log2 :: LLVMVar -> Maybe LLVMVar
log2 (I i)
  | i <= 0     = Nothing
  | i == 1     = Just $ I 0
  | mod i 2 == 0 = case log2 (I $ div i 2) of {(Just (I i')) -> Just (I (i' + 1)); _ -> Nothing}
  | otherwise  = Nothing
log2 _ = Nothing

instance Show LLVMInstruction where
  show (Label l) = show l ++ ":"
  show (ReturnVoid) = "\tret void"
  show (Return t x) = unwords [ "\tret", show t, show x ]

  show (BinOp v op v1 v2) = case (op, log2 v1, log2 v2) of {
    (QTIMES, Just (I i), _) -> unwords [ "\t" ++ show v, "= shl", show Int32, show v2 ++ ",", show i ];
    (QTIMES, _, Just (I i)) -> unwords [ "\t" ++ show v, "= shl", show Int32, show v1 ++ ",", show i ];
    (QDIV, _, Just (I i))   -> unwords [ "\t" ++ show v, "= ashr", show Int32, show v1 ++ ",", show i ];
    _                       -> unwords [ "\t" ++ show v, "=", operation, show Int32, show v1 ++ ",", show v2 ];
  } where operation = case op of { QADD -> "add"; QSUB -> "sub"; QTIMES -> "mul"; QDIV -> "sdiv"; QMOD -> "srem" }

  show (CmpOp v op t v1 v2) = unwords [ "\t" ++ show v, "= icmp", show op, show t, show v1 ++ ", " ++ show v2 ]
  show (UnaryOp v QNEG v1) = unwords [ "\t" ++ show v, "= sub", show Int32, "0,", show v1 ]
  show (UnaryOp v QNOT v1) = unwords [ "\t" ++ show v, "= xor", show Int1, "1,", show v1 ]
  show (LoadString v v1 len) = unwords [ "\t" ++ show v, "= bitcast", "[" ++ show len ++ " x i8]*", show v1, "to i8*" ]
  show (CallRes v t l args) = unwords [ "\t" ++ show v, "= call", show t, show l ++ "(" ++ showArgs args ++ ")"]
  show (Call t l args) = unwords [ "\tcall", show t, show l ++ "(" ++ showArgs args ++ ")"]
  show (Phi v t phi) = unwords [ "\t" ++ show v, "= phi", show t, showPhi phi ]
    where showPhi phi = intercalate ", " $ Prelude.map (\(v, l) -> "[" ++ show v ++ ", %" ++ show l ++ "]" ) phi
  show (Jmp l) = "\tbr label %" ++ show l
  show (Br v l1 l2) = "\tbr i1 " ++ show v ++ ", label %" ++ show l1 ++ ", label %" ++ show l2


type LLVMBlock = [LLVMInstruction]
type LLVMHeap = Map String LLVMLab
type LLVMFunction = Map LLVMLab (LLVMType, [(LLVMType, LLVMVar)], [LLVMLab])

data LLVMProgram = LLVM LLVMHeap LLVMFunction (Map LLVMLab LLVMBlock) deriving (Eq, Ord)

instance Show LLVMProgram where
  show prog@(LLVM heap function blocks) = intercalate "\n\n" lines
    where
      lines = [lib] ++ (Prelude.map showHeapElem $ Map.toList heap) ++ (Prelude.map showFunction $ Map.toList function)

      lib = unlines $ snd $ unzip $ Prelude.filter filterFunctionUsed libFunctions

      filterFunctionUsed (fun, _) = [] /= allInstructions (filterInstructions (filterFunctionCall fun) prog)

      filterFunctionCall fun (CallRes _ _ fun' _) = fun == fun'
      filterFunctionCall fun (Call _ fun' _) = fun == fun'
      filterFunctionCall _ _ = False

      libFunctions =
        [ (L "@readInt", "declare i32 @readInt()")
        , (L "@printInt", "declare void @printInt(i32)")
        , (L "@readString", "declare i8* @readString()")
        , (L "@printString", "declare void @printString(i8*)")
        , (L "@error", "declare void @error()")
        , (L "@concat", "declare i8* @concat(i8*, i8*)")
        ]

      showHeapElem (str, label) = unwords
        [ show label, "= internal constant", "[" ++ (show $ lenStr str) ++ " x i8]", showStr str]

      showFunction (label, (t, args, bLabels)) = unlines $
        [ unwords [ "define", show t, (show label) ++ "(" ++ showArgs args ++ ") {" ] ] ++
        (Prelude.map showBlock $ catMaybes $ Prelude.map (\l -> Map.lookup l blocks) bLabels) ++ [ "}" ]
      showBlock code = (intercalate "\n" $ Prelude.map show code)

lenStr str = (div (length (showStr str)) 3) - 1
showStr str = "c\"" ++ parseStr str ++ "\\00\""
parseStr str = case readLitChar str of
  [(c, str')] -> let hex = Prelude.map toUpper (showHex (ord c) "") in
    "\\" ++ (if length hex == 1 then "0" ++ hex else hex) ++ parseStr str';
  _ -> str;

showArgs args = intercalate ", " $ Prelude.map (\(t, v) -> show t ++ " " ++ show v ) args

mapInstructions :: (LLVMInstruction -> LLVMInstruction) -> LLVMProgram -> LLVMProgram
mapInstructions f (LLVM heap function blocks) = (LLVM heap function blocks')
  where blocks' = Map.map (Prelude.map f) blocks

mapInstructionsVars :: (LLVMVar -> LLVMVar) -> LLVMProgram -> LLVMProgram
mapInstructionsVars f prog = mapInstructions f' prog
  where f' instr = case instr of
          (Label l)           -> Label l
          (ReturnVoid)        -> ReturnVoid
          (Return t v)        -> Return t (f v)
          (BinOp v op v1 v2)  -> BinOp (f v) op (f v1) (f v2)
          (CmpOp v op t v1 v2)-> CmpOp (f v) op t (f v1) (f v2)
          (UnaryOp v op v1)   -> UnaryOp (f v) op (f v1)
          (LoadString v l i)  -> LoadString (f v) l i
          (Call t l args)     -> Call t l $ Prelude.map (\(_t, _v) -> (_t, f _v)) args
          (CallRes v t l args)-> CallRes (f v) t l $ Prelude.map (\(_t, _v) -> (_t, f _v)) args
          (Phi v t phi)       -> Phi (f v) t $ Prelude.map (\(_v, _l) -> (f _v, _l)) phi
          (Jmp l)             -> Jmp l
          (Br v l1 l2)        -> Br (f v) l1 l2


filterInstructions :: (LLVMInstruction -> Bool) -> LLVMProgram -> LLVMProgram
filterInstructions f (LLVM heap function blocks) = (LLVM heap function blocks')
  where blocks' = Map.map (Prelude.filter f) blocks

nextBlocks :: LLVMProgram -> LLVMLab -> [LLVMLab]
nextBlocks (LLVM _ _ blocks) label = case Map.lookup label blocks of
  Just block -> case last block of
    (Jmp v) -> [v]
    (Br _ v1 v2) -> [v1, v2]
    _ -> []
  Nothing -> []

allInstructions :: LLVMProgram -> [LLVMInstruction]
allInstructions (LLVM _ _ blocks) = concat $ Map.elems blocks
