module BackEnd.Generate where

import Control.Monad.State
import Data.Map.Strict as Map
import Data.Maybe
import Data.Set as Set

import Latte.AbsLatte

import FrontEnd.Common

import BackEnd.Common

type GlobalTState = (Integer, Maybe LLVMLab, Maybe LLVMLab, LLVMProgram)

type LocalTState = Map Ident (LLVMVar, LLVMType)

generate :: TypedProgram -> IO LLVMProgram
generate (Program _ defs) = do
  let generator = Prelude.map generateFunction defs
  (_, _, _, p) <- execStateT (execStateT (Prelude.foldl (>>) (return ()) generator) initLTS) initGTS
  let (LLVM heap fun blocks) = p in
    return $ LLVM heap (Map.map (\(t, args, labels) -> (t, args, reverse labels)) fun) (Map.map reverse blocks)

initGTS :: GlobalTState
initGTS = (0, Nothing, Nothing, LLVM Map.empty Map.empty Map.empty) -- number, current function, current label, program

initLTS :: LocalTState
initLTS = Map.empty

generateFunction :: TypedTopDef -> StateT LocalTState (StateT GlobalTState IO) ()
generateFunction (FnDef _ t ident args block) = do
  put Map.empty
  id <- getFunctionId ident
  args' <- sequence $ Prelude.map generateArg args
  beginNewFunction (getLLVMType t) id args'
  getNewBlockId >>= beginNewBlock
  generateBlock block

generateArg :: TypedArg -> StateT LocalTState (StateT GlobalTState IO) (LLVMType, LLVMVar)
generateArg (Arg _ t ident) = do
  id <- getNewVariableId
  putVariableMapping ident (getLLVMType t) id
  return (getLLVMType t, id)

generateBlock :: TypedBlock -> StateT LocalTState (StateT GlobalTState IO) ()
generateBlock (Block _ stmts) = Prelude.foldl (>>) (return ()) (Prelude.map generateStmt stmts)

generateStmt :: TypedStmt -> StateT LocalTState (StateT GlobalTState IO) ()
generateStmt stmt = case stmt of
  (Empty _)           -> return ()
  (BStmt _ block)     -> generateBlock block
  (Decl _ _ items)    -> Prelude.foldl (>>) (return ()) $ Prelude.map generateItem items
  (Ass t ident expr)  -> generateExpr expr >>= putVariableMapping ident (getLLVMType (getExprA expr))
  (Incr _ ident)  -> do
    let expr = EAdd (Int ()) (EVar (Int ()) ident) (Plus (Void ())) (ELitInt (Int ()) 1)
    generateStmt (Ass (Void ()) ident expr)
  (Decr _ ident)  -> do
    let expr = EAdd (Int ()) (EVar (Int ()) ident) (Minus (Void ())) (ELitInt (Int ()) 1)
    generateStmt (Ass (Void ()) ident expr)
  (Ret _ expr)    -> generateExpr expr >>= \id -> addInstruction (Return (getLLVMType (getExprA expr)) id)
  (VRet _)        -> addInstruction (ReturnVoid)

  (Cond _ expr s) -> do
    condId  <- generateExpr expr
    case condId of
      (B True) -> do
        generateStmt s
      (B False) -> do
        return ()
      _ -> do
        currentId <- getCurrentBlock
        trueId <- getNewBlockId
        afterId <- getNewBlockId
        addInstruction (Br condId trueId afterId)

        mapping <- get
        beginNewBlock trueId
        mapping' <- lift $ execStateT (generateStmt s) mapping
        b <- hasCurrentBlockEnded
        trueId' <- getCurrentBlock
        if not b then addInstruction (Jmp afterId) else return ()

        beginNewBlock afterId
        if not b then addPhi currentId mapping trueId' mapping' else return ()

  (CondElse _ expr s1 s2) -> do
    condId  <- generateExpr expr
    case condId of
      (B True) -> do
        generateStmt s1
      (B False) -> do
        generateStmt s2
      _ -> do
        currentId <- getCurrentBlock
        trueId <- getNewBlockId
        falseId <- getNewBlockId
        afterId <- getNewBlockId
        addInstruction (Br condId trueId falseId)

        beginNewBlock trueId
        mapping1 <- get >>= \mapping -> lift $ execStateT (generateStmt s1) mapping
        b1 <- hasCurrentBlockEnded
        if not b1 then addInstruction (Jmp afterId) else return ()
        trueId' <- getCurrentBlock

        beginNewBlock falseId
        mapping2 <- get >>= \mapping -> lift $ execStateT (generateStmt s2) mapping
        b2 <- hasCurrentBlockEnded
        if not b2 then addInstruction (Jmp afterId) else return ()
        falseId' <- getCurrentBlock

        if b1 && b2 then return () else do
          beginNewBlock afterId
          if not b1 then put mapping1 else return ()
          if not b2 then put mapping2 else return ()
          if not b1 && not b2 then addPhi trueId' mapping1 falseId' mapping2 else return ()

  (While _ expr s) -> do
    currentId <- getCurrentBlock
    checkId <- getNewBlockId
    loopId <- getNewBlockId
    afterId <- getNewBlockId
    addInstruction (Jmp checkId)

    beginNewBlock checkId
    mapping <- get
    addFullPhi currentId mapping loopId
    mapping' <- get
    condId  <- generateExpr expr
    addInstruction (Br condId loopId afterId)

    beginNewBlock loopId
    mapping'' <- lift $ execStateT (generateStmt s) mapping'
    b <- hasCurrentBlockEnded
    loopId' <- getCurrentBlock
    if not b then addInstruction (Jmp checkId) else return ()

    fixPhi b mapping mapping' mapping''
    let mapPhi instr = case instr of {
      (Phi v t [(v1, l1), (v2, l2)]) | l1 == currentId && l2 == loopId -> Phi v t [(v1, currentId), (v2, loopId')];
      _ -> instr
    }
    getProg >>= return . (mapInstructions mapPhi) >>= putProg

    beginNewBlock afterId

  (SExp _ expr) -> generateExpr expr >> return ()

generateItem :: TypedItem -> StateT LocalTState (StateT GlobalTState IO) ()
generateItem item = case item of
  (Init _ ident expr) -> generateStmt (Ass (Void ()) ident expr) -- "no init" is an impossible case

generateExpr :: TypedExpr -> StateT LocalTState (StateT GlobalTState IO) LLVMVar
generateExpr expr = case expr of
  (EVar _ ident)  -> getVariableMapping ident >>= (return . fst)
  (ELitInt _ i)   -> return (I i)
  (ELitTrue _)    -> return (B True)
  (ELitFalse _)   -> return (B False)
  (EApp t ident exprs) -> do
    labels <- sequence $ Prelude.map generateExpr exprs
    let typedLabels = Prelude.map (\(e, l) -> (getLLVMType (getExprA e), l)) (zip exprs labels)
    functionId <- getFunctionId ident
    id <- getNewVariableId
    let t' = getLLVMType t
    if t' == V0
      then addInstruction (Call t' functionId typedLabels)
      else addInstruction (CallRes id t' functionId typedLabels)
    return id
  (EString _ str) -> do
    stringId <- addString str
    id <- getNewVariableId
    addInstruction (LoadString id stringId $ lenStr str)
    return id
  (Neg _ _) -> generateUnaryExpr expr
  (Not _ _) -> generateUnaryExpr expr
  (EMul _ _ _ _)  -> generateBinaryExpr expr
  (EAdd (Str ()) e1 (Plus l) e2) -> do
    labels <- sequence $ Prelude.map generateExpr [e1, e2]
    let typedLabels = Prelude.map (\l -> (Ptr Int8, l)) labels
    id <- getNewVariableId
    addInstruction (CallRes id (getLLVMType (Str ())) (L "@concat") typedLabels)
    return id
  (EAdd _ _ _ _)  -> generateBinaryExpr expr
  (ERel _ _ _ _)  -> generateBinaryExpr expr
  (EAnd _ e1 e2)  -> do
    id1'    <- generateExpr e1
    case id1' of
      (B True) -> generateExpr e2
      (B False) -> return $ B False
      _ -> do
        curId   <- getCurrentBlock
        okId    <- getNewBlockId
        failId  <- getNewBlockId
        addInstruction (Br id1' okId failId)

        beginNewBlock okId
        id2'    <- generateExpr e2
        addInstruction (Jmp failId)
        okId'   <- getCurrentBlock

        beginNewBlock failId
        mergeId <- getNewVariableId
        addInstruction (Phi mergeId Int1 [(B False, curId), (id2', okId')])
        return mergeId

  (EOr _ e1 e2)   -> do
    id1'    <- generateExpr e1
    case id1' of
      (B True) -> return $ B True
      (B False) -> generateExpr e2
      _ -> do
        curId   <- getCurrentBlock
        failId  <- getNewBlockId
        okId    <- getNewBlockId
        addInstruction (Br id1' okId failId)

        beginNewBlock failId
        id2'    <- generateExpr e2
        addInstruction (Jmp okId)
        failId' <- getCurrentBlock

        beginNewBlock okId
        mergeId <- getNewVariableId
        addInstruction (Phi mergeId Int1 [(B True, curId), (id2', failId')])
        return mergeId

generateUnaryExpr expr = do
  id' <- generateExpr e
  if isConstant id'
    then return $ foldUnaryOp id' op
  else do
    id  <- getNewVariableId
    addInstruction (UnaryOp id op id')
    return id
    where
      (e, op) = case expr of
        (Neg _ e) -> (e, QNEG)
        (Not _ e) -> (e, QNOT)

foldUnaryOp :: LLVMVar -> UnaryOperation -> LLVMVar
foldUnaryOp id unOp = case (id, unOp) of
  ((I i), QNEG) -> I $ -i
  ((B b), QNOT) -> B $ not b
  _ -> V "error"

generateBinaryExpr expr = do
  id1' <- generateExpr e1
  id2' <- generateExpr e2

  --folding variables
  if isJust binOp && isConstant id1' && isConstant id2'
    then return $ foldBinaryOp id1' id2' (fromJust binOp)
  else if isConstant id1' && isConstant id2'
    then return $ foldCompareOp id1' id2' (fromJust cmpOp)
  -- special cases
  else if binOp == Just QSUB && id1' == id2'
    then return $ I 0
  else if binOp == Just QADD && id1' == id2' then do
    id <- getNewVariableId
    addInstruction $ BinOp id QTIMES id1' (I 2)
    return id
  else if binOp == Just QTIMES && (id1' == I 0 || id2' == I 0)
    then return $ I 0
  else if binOp == Just QTIMES && id1' == I 1
    then return id2'
  else if binOp == Just QTIMES && id2' == I 1
    then return id1'
  else if cmpOp == Just QGE && id1' == id2'
    then return $ B True
  else if cmpOp == Just QLE && id1' == id2'
    then return $ B True
  else if cmpOp == Just QEQU && id1' == id2'
    then return $ B True
  else if cmpOp == Just QGTH && id1' == id2'
    then return $ B False
  else if cmpOp == Just QLTH && id1' == id2'
    then return $ B False
  else if cmpOp == Just QNE && id1' == id2'
    then return $ B False
  -- regular cases
  else if isJust binOp then do
    id <- getNewVariableId
    addInstruction $ BinOp id (fromJust binOp) id1' id2'
    return id
  else do
    id <- getNewVariableId
    addInstruction $ CmpOp id (fromJust cmpOp) (getLLVMType (getExprA e1)) id1' id2'
    return id
    where
      (e1, e2, binOp, cmpOp) = case expr of
        (EMul _ e1 (Times _) e2)  -> (e1, e2, Just QTIMES, Nothing)
        (EMul _ e1 (Div _) e2)    -> (e1, e2, Just QDIV, Nothing)
        (EMul _ e1 (Mod _) e2)    -> (e1, e2, Just QMOD, Nothing)
        (EAdd _ e1 (Plus _) e2)   -> (e1, e2, Just QADD, Nothing)
        (EAdd _ e1 (Minus _) e2)  -> (e1, e2, Just QSUB, Nothing)
        (ERel _ e1 (LE _) e2)     -> (e1, e2, Nothing, Just QLE)
        (ERel _ e1 (LTH _) e2)    -> (e1, e2, Nothing, Just QLTH)
        (ERel _ e1 (GE _) e2)     -> (e1, e2, Nothing, Just QGE)
        (ERel _ e1 (GTH _) e2)    -> (e1, e2, Nothing, Just QGTH)
        (ERel _ e1 (EQU _) e2)    -> (e1, e2, Nothing, Just QEQU)
        (ERel _ e1 (NE _) e2)     -> (e1, e2, Nothing, Just QNE)

foldBinaryOp :: LLVMVar -> LLVMVar -> BinOperation -> LLVMVar
foldBinaryOp id1 id2 binOp = case (id1, id2, binOp) of
  ((I i1), (I i2), QADD)    -> I $ i1 + i2
  ((I i1), (I i2), QSUB)    -> I $ i1 - i2
  ((I i1), (I i2), QTIMES)  -> I $ i1 * i2
  ((I i1), (I i2), QDIV)    -> I $ div i1 i2
  ((I i1), (I i2), QMOD)    -> I $ mod i1 i2

foldCompareOp :: LLVMVar -> LLVMVar -> CmpOperation -> LLVMVar
foldCompareOp id1 id2 cmpOp = case (id1, id2, cmpOp) of
  ((I i1), (I i2), QLTH)  -> B $ i1 < i2
  ((I i1), (I i2), QLE)   -> B $ i1 <= i2
  ((I i1), (I i2), QGTH)  -> B $ i1 > i2
  ((I i1), (I i2), QGE)   -> B $ i1 >= i2
  (_, _, QEQU)  -> B $ id1 == id2
  (_, _, QNE)   -> B $ id1 /= id2

addPhi :: LLVMLab -> LocalTState -> LLVMLab -> LocalTState -> StateT LocalTState (StateT GlobalTState IO) ()
addPhi l1 m1 l2 m2 = do
  sequence (Prelude.map phiSingle (assocs $ intersectionWith (\a b -> (a, b)) m1 m2)) >> return ()
    where
      phiSingle :: (Ident, ((LLVMVar, LLVMType), (LLVMVar, LLVMType))) -> StateT LocalTState (StateT GlobalTState IO) ()
      phiSingle (k, ((v1, t1), (v2, t2))) = if v1 == v2 then return () else do
        v' <- getNewVariableId
        addInstruction (Phi v' t1 [(v1, l1), (v2, l2)])
        putVariableMapping k t1 v'

addFullPhi :: LLVMLab -> LocalTState -> LLVMLab -> StateT LocalTState (StateT GlobalTState IO) ()
addFullPhi l1 m l2 = do
  sequence (Prelude.map phiSingle (assocs m)) >> return ()
    where
      phiSingle :: (Ident, (LLVMVar, LLVMType)) -> StateT LocalTState (StateT GlobalTState IO) ()
      phiSingle (k, (v, t)) = do
        v' <- getNewVariableId
        addInstruction (Phi v' t [(v, l1), (v', l2)])
        putVariableMapping k t v'

fixPhi :: Bool -> LocalTState -> LocalTState -> LocalTState -> StateT LocalTState (StateT GlobalTState IO) ()
fixPhi b m m' m'' = do
  let common = Map.assocs $ intersectionWith (:) m $ intersectionWith (\a b -> [a, b]) m' m''
  sequence (Prelude.map fixSingle common) >> return ()
    where
      fixSingle :: (Ident, [(LLVMVar, LLVMType)]) -> StateT LocalTState (StateT GlobalTState IO) ()
      fixSingle (k, [(v0, t), (v1, _), (v2, _)]) = getProg >>= \prog -> if v1 == v2 || b
        then do
          let prog' = mapInstructionsVars (revertPhi v1 v0) $ filterInstructions (unnecessaryPhi v1) prog
          putProg prog' >> putVariableMapping k t v0
        else putProg $ mapInstructions (changePhi v1 v2) prog

      unnecessaryPhi :: LLVMVar -> LLVMInstruction -> Bool
      unnecessaryPhi v instr = case instr of
        (Phi _v _t [(_v1, _l1), (_v2, _l2)]) | _v == v -> False
        _ -> True

      revertPhi :: LLVMVar -> LLVMVar -> LLVMVar -> LLVMVar
      revertPhi _v1 _v2 v = if v == _v1 then _v2 else v

      changePhi :: LLVMVar -> LLVMVar -> LLVMInstruction -> LLVMInstruction
      changePhi v1 v2 instr = case instr of
        (Phi _v _t [(_v1, _l1), (_v2, _l2)]) | _v == v1 -> Phi _v _t [(_v1, _l1), (v2, _l2)]
        _ -> instr

addInstruction :: LLVMInstruction -> StateT LocalTState (StateT GlobalTState IO) ()
addInstruction instr = getCurrentBlock >>= \current -> do
  code <- getBlock current
  putBlock current (instr:code)

beginNewFunction :: LLVMType -> LLVMLab -> [(LLVMType, LLVMVar)] -> StateT LocalTState (StateT GlobalTState IO) ()
beginNewFunction t label args = do
  (LLVM heap functions blocks) <- getProg
  putProg (LLVM heap (Map.insert label (t, args, []) functions) blocks)
  putCurrentFunction label

addString :: String -> StateT LocalTState (StateT GlobalTState IO) LLVMLab
addString str = do
  stringId <- lookupHeap str >>= (\v -> case v of
    (Just id') -> return id'
    Nothing    -> getNewHeapId >>= \id' -> insertHeap str id' >> return id')
  return stringId

lookupHeap :: String -> StateT LocalTState (StateT GlobalTState IO) (Maybe LLVMLab)
lookupHeap s = getProg >>= \(LLVM h _ _) -> return $ Map.lookup s h

insertHeap :: String -> LLVMLab -> StateT LocalTState (StateT GlobalTState IO) ()
insertHeap s v = do
  (LLVM heap functions blocks) <- getProg
  putProg (LLVM (Map.insert s v heap) functions blocks)

beginNewBlock :: LLVMLab -> StateT LocalTState (StateT GlobalTState IO) ()
beginNewBlock id = do
  funLabel <- getCurrentFunction
  (LLVM heap functions blocks) <- getProg
  let (t, args, labels) = fromJust $ Map.lookup funLabel functions
  let functions' = Map.insert funLabel (t, args, id:labels) functions
  putProg (LLVM heap functions' blocks) >> putCurrentBlock id >> putBlock id [Label id]

getFunctionId :: Ident -> StateT LocalTState (StateT GlobalTState IO) LLVMLab
getFunctionId (Ident name) = do
  if elem name ["printInt", "printString", "error", "readInt", "readString", "main"]
    then return $ L ("@" ++ name)
    else return $ L ("@__" ++ name)

getNewHeapId :: StateT LocalTState (StateT GlobalTState IO) LLVMLab
getNewHeapId = getNewId >>= \id -> return $ L ("@s" ++ id)

getNewBlockId :: StateT LocalTState (StateT GlobalTState IO) LLVMLab
getNewBlockId = getNewId >>= \id -> return $ L ("L" ++ id)

getNewVariableId :: StateT LocalTState (StateT GlobalTState IO) LLVMVar
getNewVariableId = getNewId >>= \id -> return $ V ("t" ++ id)

getNewId :: StateT LocalTState (StateT GlobalTState IO) String
getNewId = getNum >>= \num -> putNum (num + 1) >> return (show num)

hasCurrentBlockEnded :: StateT LocalTState (StateT GlobalTState IO) Bool
hasCurrentBlockEnded = getCurrentBlock >>= getBlock >>= \code -> do
  case head code of {(Return _ _) -> return True; ReturnVoid -> return True; _ -> return False}

-- State getters/putters

getNum :: StateT LocalTState (StateT GlobalTState IO) Integer
getNum = lift $ get >>= \(i, _, _, _) -> return i

putNum :: Integer -> StateT LocalTState (StateT GlobalTState IO) ()
putNum i = lift $ modify (\(_, b, c, d) -> (i, b, c, d))

getVariableMapping :: Ident -> StateT LocalTState (StateT GlobalTState IO) (LLVMVar, LLVMType)
getVariableMapping ident = get >>= \mapping -> return $ fromJust $ Map.lookup ident mapping

putVariableMapping :: Ident -> LLVMType -> LLVMVar -> StateT LocalTState (StateT GlobalTState IO) ()
putVariableMapping ident t label = get >>= put . (Map.insert ident (label, t))

getCurrentFunction :: StateT LocalTState (StateT GlobalTState IO) LLVMLab
getCurrentFunction = lift $ get >>= \(_, c, _, _) -> return $ fromJust c

putCurrentFunction :: LLVMLab -> StateT LocalTState (StateT GlobalTState IO) ()
putCurrentFunction current = lift $ modify (\(a, _, c, d) -> (a, Just current, c, d))

getCurrentBlock :: StateT LocalTState (StateT GlobalTState IO) LLVMLab
getCurrentBlock = lift $ get >>= \(_, _, c, _) -> return $ fromJust c

putCurrentBlock :: LLVMLab -> StateT LocalTState (StateT GlobalTState IO) ()
putCurrentBlock current = lift $ modify (\(a, b, _, d) -> (a, b, Just current, d))

getProg :: StateT LocalTState (StateT GlobalTState IO) LLVMProgram
getProg = lift $ get >>= \(_, _, _, p) -> return p

putProg :: LLVMProgram -> StateT LocalTState (StateT GlobalTState IO) ()
putProg p = lift $ modify (\(a, b, c, _) -> (a, b, c, p))

getBlocks :: StateT LocalTState (StateT GlobalTState IO) (Map LLVMLab LLVMBlock)
getBlocks = getProg >>= \(LLVM _ _ m) -> return m

putBlocks :: (Map LLVMLab LLVMBlock) -> StateT LocalTState (StateT GlobalTState IO) ()
putBlocks m = getProg >>= \(LLVM a b _) -> putProg (LLVM a b m)

getBlock :: LLVMLab -> StateT LocalTState (StateT GlobalTState IO) LLVMBlock
getBlock label = getBlocks >>= \blocks -> return $ fromJust $ Map.lookup label blocks

putBlock :: LLVMLab -> LLVMBlock -> StateT LocalTState (StateT GlobalTState IO) ()
putBlock label b = getBlocks >>= \blocks -> putBlocks (Map.insert label b blocks)
