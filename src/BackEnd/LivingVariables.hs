module BackEnd.LivingVariables where

import Control.Monad.State
import Data.Map.Strict as Map
import Data.Maybe
import Data.Set as Set

import BackEnd.Common


run :: LLVMProgram -> IO LLVMProgram
run prog = do
  aliveVariables <- alive prog
  let prog' = mapInstructions (mapCall aliveVariables) $ filterInstructions (aliveInstr aliveVariables) prog
  return prog'
    where
      aliveInstr aliveVar instr = case instr of
        (BinOp v _ _ _)     -> Set.member v aliveVar
        (CmpOp v _ _ _ _)   -> Set.member v aliveVar
        (UnaryOp v _ _)     -> Set.member v aliveVar
        (LoadString v _ _)  -> Set.member v aliveVar
        (Phi v _ _)         -> Set.member v aliveVar
        _ -> True

      mapCall aliveVar instr = case instr of
        (CallRes v t l vs) -> if Set.member v aliveVar then instr else Call t l vs
        _ -> instr

type AliveVar = Set LLVMVar
type AliveState = (AliveVar, Map LLVMLab (AliveVar, AliveVar))

alive :: LLVMProgram -> IO AliveVar
alive prog = execStateT (aliveTurn prog) (initState prog) >>= return . fst

initState :: LLVMProgram -> AliveState
initState (LLVM _ _ blocks) = (Set.empty, Map.map (\_ -> (Set.empty, Set.empty)) blocks)

aliveTurn :: LLVMProgram -> StateT AliveState IO ()
aliveTurn prog@(LLVM _ _ blocks) = do
  state <- get
  sequence $ Prelude.map (aliveBlock prog) (Map.keys blocks)
  state' <- get
  if state == state' then return () else aliveTurn prog

aliveBlock :: LLVMProgram -> LLVMLab -> StateT AliveState IO ()
aliveBlock prog@(LLVM _ _ blocks) label = do
  let code = fromJust $ Map.lookup label blocks
  let pass = sequence $ reverse $ Prelude.map (aliveInstr label) code

  endingStates <- sequence $ (getBlockEndAlive label):(Prelude.map getBlockBeginAlive (nextBlocks prog label))
  let endingState = Set.unions endingStates
  putBlockEndAlive label endingState

  beginState <- execStateT pass endingState
  putBlockBeginAlive label beginState

aliveInstr :: LLVMLab -> LLVMInstruction -> StateT AliveVar (StateT AliveState IO) ()
aliveInstr label instr = do
  get >>= lift . addAliveVars

  case instr of
    (Return _ v)        -> addVars [v]
    (BinOp v _ v1 v2)   -> addCondVars v [v1, v2]
    (CmpOp v _ _ v1 v2) -> addCondVars v [v1, v2]
    (UnaryOp v _ v1)    -> addCondVars v [v1]
    (LoadString v _ _)  -> deleteVar v
    (Call _ _ args)     -> addVars $ snd $ unzip args
    (CallRes v _ _ args)-> deleteVar v >> addVars (snd (unzip args))
    (Phi v _ phi)       -> let [(v1, l1), (v2, l2)] = phi in get >>= \s -> if Set.member v s
      then deleteVar v >> lift (addEndVars l1 [v1]) >> lift (addEndVars l2 [v2]) else return ()
    (Br v _ _)          -> addVars [v]
    _ -> return ()

  get >>= lift . addAliveVars

addEndVars :: LLVMLab -> [LLVMVar] -> StateT AliveState IO ()
addEndVars label vars = getBlockEndAlive label >>= (putBlockEndAlive label) . (Set.union (Set.fromList vars))

addVars :: [LLVMVar] -> StateT AliveVar (StateT AliveState IO) ()
addVars vars = modify $ Set.union $ Set.fromList vars

deleteVar :: LLVMVar -> StateT AliveVar (StateT AliveState IO) ()
deleteVar v = modify $ Set.delete v

addCondVars ::  LLVMVar -> [LLVMVar] -> StateT AliveVar (StateT AliveState IO) ()
addCondVars lvalue rvalues = get >>= \s ->
  if Set.member lvalue s then deleteVar lvalue >> addVars rvalues else return ()

-- getters/putters

getBlockBeginAlive :: LLVMLab -> StateT AliveState IO AliveVar
getBlockBeginAlive label = getBlockData label >>= \(x, _) -> return x

putBlockBeginAlive :: LLVMLab -> AliveVar -> StateT AliveState IO ()
putBlockBeginAlive label begin = modifyBlockData label (\(_, y) -> (begin, y))

getBlockEndAlive :: LLVMLab -> StateT AliveState IO AliveVar
getBlockEndAlive label = getBlockData label >>= \(_, x) -> return x

putBlockEndAlive :: LLVMLab -> AliveVar -> StateT AliveState IO ()
putBlockEndAlive label end = modifyBlockData label (\(x, _) -> (x, end))

addAliveVars :: AliveVar -> StateT AliveState IO ()
addAliveVars alive = modify (\(all, y) -> (Set.union all alive, y))

getBlockData :: LLVMLab -> StateT AliveState IO (AliveVar, AliveVar)
getBlockData label = get >>= \(_, bData) -> return $ fromJust $ Map.lookup label bData

modifyBlockData :: LLVMLab -> ((AliveVar, AliveVar) -> (AliveVar, AliveVar)) -> StateT AliveState IO ()
modifyBlockData label f = getBlockData label >>= \b -> modify (\(x, bData) -> (x, Map.insert label (f b) bData))
