module BackEnd.Printer where

import Data.List
import Data.Map.Strict as Map

import System.Exit hiding (die)
import System.FilePath.Posix
import System.IO
import System.Process

import BackEnd.Common

save :: String -> LLVMProgram -> IO ()
save filename prog = do
  let code = show prog
  let filename' = replaceExtension filename "ll"
  saveProgram filename' code
  linkLLVM filename'

saveProgram :: String -> String -> IO()
saveProgram filename code = writeFile filename code

usage = hPutStrLn stderr "Usage: ./latc_llvm {program}.lat"
exit  = exitWith ExitSuccess
die   = exitWith (ExitFailure 1)

runCommandWithExit :: String -> IO ()
runCommandWithExit command = runInteractiveCommand command
  >>= \(_pIn, pOut, pErr, handle) -> waitForProcess handle
  >>= \exitCode -> case exitCode of
    ExitSuccess -> return ()
    (ExitFailure _) -> do
      hPutStrLn stderr "ERROR"
      hGetContents pOut >>= hPutStrLn stderr
      hGetContents pErr >>= hPutStrLn stderr
      die

runCommandsWithExit :: [String] -> IO()
runCommandsWithExit [] = return ()
runCommandsWithExit (command:commands) = runCommandWithExit command >> runCommandsWithExit commands

linkLLVM :: String -> IO ()
linkLLVM filename = do
  let runtimeFilename = "lib/runtime.bc"
  let tmpFilename = replaceExtension filename "tmp"
  let destFilename = replaceExtension filename "bc"

  runCommandsWithExit
    [ "llvm-as -o " ++ tmpFilename ++ " " ++ filename
    , "llvm-link -o " ++ destFilename ++ " " ++ tmpFilename ++ "  " ++ runtimeFilename
    , "rm -rf " ++ tmpFilename
    ]
