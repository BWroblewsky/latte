module BackEnd.LivingStrings where

import Data.Map.Strict as Map
import Data.Maybe
import Data.Set as Set

import BackEnd.Common

run :: LLVMProgram -> IO LLVMProgram
run prog@(LLVM heap function blocks) = do
  let strings = Set.fromList $ catMaybes $ Prelude.map getString $ allInstructions prog
  let heap' = Map.filter (\s -> Set.member s strings) heap
  return (LLVM heap' function blocks)
    where
      getString :: LLVMInstruction -> Maybe LLVMLab
      getString (LoadString _ l _) = Just l
      getString _ = Nothing
