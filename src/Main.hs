module Main where

import System.IO

import Latte.PrintLatte (printTree)

import FrontEnd.Parser (getFilename, parseProgram)
import FrontEnd.ReduceConstants (run)
import FrontEnd.ReduceUnreachable (run)
import FrontEnd.SimplifyNaming (run)
import FrontEnd.ValidateMain (run)
import FrontEnd.ValidateTyping (run)
import FrontEnd.ValidateUnique (run)

import BackEnd.Generate (generate)
import BackEnd.LivingStrings (run)
import BackEnd.LivingVariables (run)
import BackEnd.Printer (save)

main :: IO ()
main = do
  filename <- getFilename
  tree <- foldl (>>=) (parseProgram filename)
    [ FrontEnd.ValidateMain.run
    , FrontEnd.ValidateUnique.run
    ]
  typedTree <- foldl (>>=) (FrontEnd.ValidateTyping.run tree)
    [ FrontEnd.ReduceConstants.run
    , FrontEnd.ReduceUnreachable.run
    , FrontEnd.SimplifyNaming.run
    ]

  llvmCode <- foldl (>>=) (BackEnd.Generate.generate typedTree)
    [ BackEnd.LivingVariables.run
    , BackEnd.LivingStrings.run
    ]
  save filename llvmCode

  hPutStrLn stderr "OK"
