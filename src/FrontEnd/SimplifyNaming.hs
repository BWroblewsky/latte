module FrontEnd.SimplifyNaming where

import Control.Monad.State
import Data.Map.Strict as Map

import Latte.AbsLatte

import FrontEnd.Common

run :: TypedProgram -> IO TypedProgram
run (Program _t definitions) = return (Program _t (Prelude.map simplifyNamingDefinition definitions))

simplifyNamingDefinition :: TypedTopDef -> TypedTopDef
simplifyNamingDefinition (FnDef _t t ident args block) = FnDef _t t ident args' block'
  where
    (args', s)  = runState (sequence (Prelude.map simplifyNamingArg args)) (0, empty)
    block' = evalState (simplifyNamingBlock block) s

simplifyNamingArg :: TypedArg -> State (Integer, Map Ident Integer) TypedArg
simplifyNamingArg (Arg _t t ident) = newIdent ident >>= \ident' -> return (Arg _t t ident')

simplifyNamingBlock :: TypedBlock -> State (Integer, Map Ident Integer) TypedBlock
simplifyNamingBlock (Block _t stmts) = get >>= \st ->
  let stmts' = evalState (sequence $ Prelude.map simplifyNamingStmt stmts) st in
  return (Block _t stmts')

simplifyNamingStmt :: TypedStmt -> State (Integer, Map Ident Integer) TypedStmt
simplifyNamingStmt stmt = case stmt of
  (BStmt _t block)   -> simplifyNamingBlock block >>= \block' -> return (BStmt _t block')
  (Decl _t t items)  -> do
    items' <- sequence $ Prelude.map simplifyNamingItem items
    return (Decl _t t items')
  (Ass _t ident expr) -> do
    ident' <- getIdent ident
    expr' <- simplifyNamingExpr expr
    return (Ass _t ident' expr')
  (Incr _t ident)  -> getIdent ident >>= \ident' -> return (Incr _t ident')
  (Decr _t ident)  -> getIdent ident >>= \ident' -> return (Decr _t ident')
  (Ret _t expr)    -> simplifyNamingExpr expr >>= \expr' -> return (Ret _t expr')
  (Cond _t expr s) -> do
    expr' <- simplifyNamingExpr expr
    s' <- simplifyNamingStmt s
    return (Cond _t expr' s')
  (CondElse _t expr s1 s2) -> do
    expr' <- simplifyNamingExpr expr
    s1' <- simplifyNamingStmt s1
    s2' <- simplifyNamingStmt s2
    return (CondElse _t expr' s1' s2')
  (While _t expr s) -> do
    expr' <- simplifyNamingExpr expr
    s' <- simplifyNamingStmt s
    return (While _t expr' s')
  (SExp _t expr) -> simplifyNamingExpr expr >>= \expr' -> return (SExp _t expr')
  _ -> return stmt

simplifyNamingItem :: TypedItem -> State (Integer, Map Ident Integer) TypedItem
simplifyNamingItem item = case item of
  (NoInit _t ident) -> newIdent ident >>= \ident' -> return (NoInit _t ident')
  (Init _t ident expr) -> do
    expr' <- simplifyNamingExpr expr
    ident' <- newIdent ident
    return (Init _t ident' expr')

simplifyNamingExpr :: TypedExpr -> State (Integer, Map Ident Integer) TypedExpr
simplifyNamingExpr expr = case expr of
  (EVar _t ident) -> getIdent ident >>= \ident' -> return (EVar _t ident')
  (EApp _t ident exprs) -> sequence (Prelude.map simplifyNamingExpr exprs) >>= \exprs' -> return (EApp _t ident exprs')
  (Neg _t e) -> simplifyNamingExpr e >>= \e' -> return (Neg _t e')
  (Not _t e) -> simplifyNamingExpr e >>= \e' -> return (Not _t e')
  (EMul _t e1 op e2) -> do
    e1' <- simplifyNamingExpr e1
    e2' <- simplifyNamingExpr e2
    return (EMul _t e1' op e2')
  (EAdd _t e1 op e2) -> do
    e1' <- simplifyNamingExpr e1
    e2' <- simplifyNamingExpr e2
    return (EAdd _t e1' op e2')
  (ERel _t e1 op e2) -> do
    e1' <- simplifyNamingExpr e1
    e2' <- simplifyNamingExpr e2
    return (ERel _t e1' op e2')
  (EAnd _t e1 e2) -> do
    e1' <- simplifyNamingExpr e1
    e2' <- simplifyNamingExpr e2
    return (EAnd _t e1' e2')
  (EOr _t e1 e2) -> do
    e1' <- simplifyNamingExpr e1
    e2' <- simplifyNamingExpr e2
    return (EOr _t e1' e2')
  _ -> return expr

newIdent :: Ident -> State (Integer, Map Ident Integer) Ident
newIdent ident = get >>= \(cnt, m) -> put (cnt + 1, Map.insert ident cnt m) >> getIdent ident

getIdent :: Ident -> State (Integer, Map Ident Integer) Ident
getIdent ident = get >>= \(_, m) -> case Map.lookup ident m of
  (Just i)  -> return (Ident ("v" ++ show i))
  Nothing   -> return (Ident "UNKNOWN")
