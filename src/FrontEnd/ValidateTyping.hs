module FrontEnd.ValidateTyping where

import Control.Monad.State
import Data.Map.Strict as Map

import Latte.AbsLatte

import FrontEnd.Common

type TypeState = Map Ident (Type ())

run :: LatteProgram -> IO TypedProgram
run prog@(Program loc defs) = do
  defs' <- sequence $ Prelude.map (\def -> evalStateT (validateTypingDefinition def) (globalTypes prog)) defs
  return $ Program (Void ()) defs'

globalTypes :: Program a -> TypeState
globalTypes (Program _ defs) = Prelude.foldl addGlobal libraryTypes defs
  where
    addGlobal typ fun@(FnDef _ _ ident _ _) = Map.insert ident (funType fun) typ

    funType (FnDef loc t _ args _) = fmap (\_ -> ()) $ Fun loc t (Prelude.map (\(Arg _ t _) -> t) args)

    libraryTypes = Map.insert (Ident "printInt") (Fun () (Void ()) [(Int ())])
                 $ Map.insert (Ident "printString") (Fun () (Void ()) [(Str ())])
                 $ Map.insert (Ident "error") (Fun () (Void ()) [])
                 $ Map.insert (Ident "readInt") (Fun () (Int ()) [])
                 $ Map.insert (Ident "readString") (Fun () (Str ()) [])
                 $ empty

validateTypingDefinition :: LatteTopDef -> StateT TypeState IO TypedTopDef
validateTypingDefinition (FnDef loc rt ident args block) = do
  let rt' = fmap (\_ -> ()) rt
  args' <- sequence $ Prelude.map validateTypingArg args
  block' <- validateTypingBlock rt' block
  return $ FnDef (Void ()) (fmap (\_ -> (Void ())) rt) ident args' block'

validateTypingArg :: LatteArg -> StateT TypeState IO TypedArg
validateTypingArg (Arg loc t ident) = case t of
  (Void _) -> lift $ fatalError loc (VoidArgException ident)
  _ -> do
    let t' = fmap (const ()) t
    modify (Map.insert ident t')
    return $ Arg t' (fmap (const (Void ())) t) ident

validateTypingBlock :: Type () -> LatteBlock -> StateT TypeState IO TypedBlock
validateTypingBlock rt (Block loc stmts) = do
  stmts' <- sequence $ Prelude.map (validateTypingStmt rt) stmts
  return (Block (Void ()) stmts')

validateTypingStmt :: Type () -> LatteStmt -> StateT TypeState IO TypedStmt
validateTypingStmt rt stmt = case stmt of
  (Empty _) -> return $ Empty (Void ())

  (BStmt loc block) -> do
    block' <- get >>= lift . (evalStateT (validateTypingBlock rt block))
    return $ BStmt (Void ()) block'

  (Ass loc ident e) -> do
    typing <- get
    case Map.lookup ident typing of
      (Just t) -> checkType t e >>= \e' -> return (Ass t ident e')
      Nothing  -> lift $ fatalError loc (VarUndeclaredException ident)

  (Incr loc ident) -> do
    typing <- get
    case Map.lookup ident typing of
      (Just (Int _))    -> return $ Incr (Void ()) ident
      (Just t)          -> lift $ fatalError loc (VarNotIncrException ident)
      Nothing           -> lift $ fatalError loc (VarUndeclaredException ident)
  (Decr loc ident) -> do
    typing <- get
    case Map.lookup ident typing of
      (Just (Int _))    -> return $ Decr (Void ()) ident
      (Just t)          -> lift $ fatalError loc (VarNotIncrException ident)
      Nothing           -> lift $ fatalError loc (VarUndeclaredException ident)

  (Ret loc e) -> do
    case rt of {(Void _) -> lift (fatalError loc ValueReturnException); _ -> return ()}
    e' <- checkType rt e
    return (Ret (Void ()) e')
  (VRet loc) -> case rt of
      (Void ()) -> return $ VRet (Void ())
      _ -> lift $ fatalError loc (VoidReturnException rt)

  (Cond _ e s) -> do
    e' <- checkType (Bool ()) e
    s' <- get >>= lift . (evalStateT (validateTypingStmt rt s))
    return $ Cond (Void ()) e' s'

  (CondElse _ e s1 s2) -> do
    e' <- checkType (Bool ()) e
    s1' <- get >>= lift . (evalStateT (validateTypingStmt rt s1))
    s2' <- get >>= lift . (evalStateT (validateTypingStmt rt s2))
    return $ CondElse (Void ()) e' s1' s2'

  (While _ e s) -> do
    e' <- checkType (Bool ()) e
    s' <- get >>= lift . (evalStateT (validateTypingStmt rt s))
    return $ While (Void ()) e' s'

  (SExp _ e) -> do
    e' <- getType e
    return $ SExp (Void ()) e'

  (Decl loc t items)  -> do
    case t of {(Void _) -> lift (fatalError loc VoidDeclException); _ -> return ()}
    items' <- sequence $ Prelude.map (validateTypingItem (fmap (\_ -> ()) t)) items
    return $ Decl (Void ()) (fmap (\_ -> Void ()) t) items'

validateTypingItem :: Type () -> LatteItem -> StateT TypeState IO TypedItem
validateTypingItem t item = case item of
  (NoInit loc ident) -> do
    modify (Map.insert ident t)
    case t of
      (Str ())  -> return (Init t ident (EString t ""))
      (Bool ()) -> return (Init t ident (ELitFalse t))
      (Int ())  -> return (Init t ident (ELitInt t 0))
  (Init loc ident e) -> do
    e' <- checkType t e
    modify $ Map.insert ident t
    return $ Init t ident e'

getType :: LatteExpr -> StateT TypeState IO TypedExpr
getType expr = case expr of
  (EVar loc ident) -> get >>= \typing -> case Map.lookup ident typing of
    (Just t) -> return (EVar t ident)
    Nothing  -> lift $ fatalError loc (VarUndeclaredException ident)

  (ELitInt _ x) -> return $ ELitInt (Int ()) x
  (ELitTrue _)  -> return $ ELitTrue (Bool ())
  (ELitFalse _) -> return $ ELitFalse (Bool ())
  (EString loc str) -> return $ EString (Str ()) $ init $ tail $ str

  (EApp loc ident exprs) -> do
    fun <- getType (EVar loc ident)
    case getExprA fun of
      (Fun _ rt ts) -> do
        if (length ts) == (length exprs)
          then return ()
          else lift $ fatalError loc (NumberOfArgException ident)

        exprs' <- sequence $ Prelude.map (\(_e, _t) -> checkType _t _e) (zip exprs ts)
        return (EApp rt ident exprs')
      _ -> lift $ fatalError loc (VarNotCallableException ident)

  (Neg loc e) -> checkType (Int ()) e >>= \e' -> return $ Neg (Int ()) e'
  (Not loc e) -> checkType (Bool ()) e >>= \e' -> return $ Not (Bool ()) e'

  (EMul _ e1 op e2) -> do
    let op' = case op of {(Times _) -> Times (Int ()); (Div _) -> Div (Int ()); (Mod _) -> Mod (Int ())}
    e1' <- checkType (Int ()) e1
    e2' <- checkType (Int ()) e2
    return $ EMul (Int ()) e1' op' e2'

  (EAdd loc e1 (Plus _) e2) -> do
    e1' <- getType e1
    e2' <- getType e2
    case (getExprA e1', getExprA e2') of
      (Int (), Int ()) -> do return $ EAdd (Int ()) e1' (Plus (Int ())) e2'
      (Str (), Str ()) -> do return $ EAdd (Str ()) e1' (Plus (Str ())) e2'
      _       -> lift $ fatalError loc (OperationUnknownException (getExprA e1'))

  (EAdd loc e1 (Minus _) e2) -> do
    e1' <- checkType (Int ()) e1
    e2' <- checkType (Int ()) e2
    return $ EAdd (Int ()) e1' (Minus (Int ())) e2'

  (ERel loc e1 (EQU _) e2) -> do
    e1' <- getType e1
    e2' <- getType e2
    case (getExprA e1', getExprA e2') of
      (Int (), Int ())   -> do return $ ERel (Bool ()) e1' (EQU (Bool ())) e2'
      (Bool (), Bool ()) -> do return $ ERel (Bool ()) e1' (EQU (Bool ())) e2'
      _       -> lift $ fatalError loc (OperationUnknownException (getExprA e1'))

  (ERel loc e1 (NE _) e2) -> do
      e1' <- getType e1
      e2' <- getType e2
      case (getExprA e1', getExprA e2') of
        (Int (), Int ())   -> do return $ ERel (Bool ()) e1' (NE (Bool ())) e2'
        (Bool (), Bool ()) -> do return $ ERel (Bool ()) e1' (NE (Bool ())) e2'
        _       -> lift $ fatalError loc (OperationUnknownException (getExprA e1'))

  (ERel _ e1 op e2) -> do
    let op' = case op of {(LTH _) -> LTH (Bool ()); (LE _) -> LE (Bool ());
                          (GTH _) -> GTH (Bool ()); (GE _) -> GE (Bool ())}
    e1' <- checkType (Int ()) e1
    e2' <- checkType (Int ()) e2
    return $ ERel (Bool ()) e1' op' e2'

  (EAnd _ e1 e2) -> do
    e1' <- checkType (Bool ()) e1
    e2' <- checkType (Bool ()) e2
    return $ EAnd (Bool ()) e1' e2'
  (EOr _ e1 e2) -> do
    e1' <- checkType (Bool ()) e1
    e2' <- checkType (Bool ()) e2
    return $ EOr (Bool ()) e1' e2'

checkType :: Type () -> LatteExpr -> StateT TypeState IO TypedExpr
checkType t1 expr = do
  expr' <- getType expr
  let t2 = getExprA expr'
  if t1 == t2
    then return expr'
    else lift $ fatalError (getExprA expr) (BadTypeException t1 t2)
