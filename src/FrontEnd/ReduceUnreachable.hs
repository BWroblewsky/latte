module FrontEnd.ReduceUnreachable where

import Control.Monad.State

import Latte.AbsLatte

import FrontEnd.Common

run :: TypedProgram -> IO TypedProgram
run (Program _t defs) = sequence (map reduceUnreachableDefinition defs) >>= \defs' -> return (Program _t defs')

reduceUnreachableDefinition :: TypedTopDef -> IO TypedTopDef
reduceUnreachableDefinition def = case def of
  (FnDef _t t ident args block) -> do
    (_block, _ok) <- runStateT (reduceUnreachableBlock block) False
    case t of
      (Void _) -> let (Block _t' stmts) = _block in do
        block' <- if _ok
          then return _block
          else return (Block _t' (stmts ++ [VRet (Void ())]))
        return (FnDef _t t ident args block')
      _ -> if _ok
        then return (FnDef _t t ident args _block)
        else fatalError Nothing (NoReturnException ident)

reduceUnreachableBlock :: TypedBlock -> StateT Bool IO TypedBlock
reduceUnreachableBlock (Block _t stmts) = do
  stmts' <- sequence $ Prelude.map reduceUnreachableStmt stmts
  return $ Block _t $ filter (\s -> case s of {(Empty _) -> False; _ -> True}) stmts'

reduceUnreachableStmt :: TypedStmt -> StateT Bool IO TypedStmt
reduceUnreachableStmt stmt = get >>= \b -> if b then return (Empty (Void ())) else case stmt of
  (BStmt _t block) -> reduceUnreachableBlock block >>= \block' -> return (BStmt _t block')
  (Ret _ _)        -> put True >> return stmt
  (VRet _)         -> put True >> return stmt
  (Cond _t expr s) -> case expr of
    (ELitTrue _)   -> reduceUnreachableStmt s
    (ELitFalse _)  -> return (Empty (Void ()))
    _              -> do
      s' <- lift (evalStateT (reduceUnreachableStmt s) False)
      return $ Cond _t expr s'
  (CondElse _t expr s1 s2) -> case expr of
    (ELitTrue _)   -> reduceUnreachableStmt s1
    (ELitFalse _)  -> reduceUnreachableStmt s2
    _              -> do
      (s1', ok1) <- lift $ runStateT (reduceUnreachableStmt s1) False
      (s2', ok2) <- lift $ runStateT (reduceUnreachableStmt s2) False
      put $ ok1 && ok2
      return $ CondElse _t expr s1' s2'
  (While _t expr s)  -> case expr of
    (ELitTrue _)  -> do
      s' <- reduceUnreachableStmt s
      b <- get
      if b then return s' else return $ While _t expr s'
    (ELitFalse _) -> return $ Empty (Void ())
    _             -> do
      s' <- lift (evalStateT (reduceUnreachableStmt s) False)
      return (While _t expr s')
  _ -> return stmt
