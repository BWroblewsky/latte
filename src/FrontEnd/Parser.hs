module FrontEnd.Parser where

import System.Directory
import System.Environment
import System.Exit
import System.IO

import Latte.ErrM
import Latte.ParLatte
import Latte.PrintLatte

import FrontEnd.Common

getFilename :: IO String
getFilename = getArgs >>= \args -> case args of {[f]  -> return f; _ -> usage >> exitFailure}

usage :: IO ()
usage = hPutStrLn stderr (unlines ["ERROR", "Usage: ./latc {filename}.lat"])

getFileContent :: String -> IO String
getFileContent filename = do
  fileExists <- doesFileExist filename
  if not fileExists
    then hPutStrLn stderr (unlines ["ERROR", "File " ++ filename ++ " does not exist"]) >> exitFailure
    else readFile filename

parseProgram :: String -> IO LatteProgram
parseProgram filename = do
  fileContent <- getFileContent filename
  case pProgram $ myLexer fileContent of
    Bad err -> do fatalError Nothing (BNFCException err)
    Ok tree -> do return tree
