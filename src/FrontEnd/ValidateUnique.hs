module FrontEnd.ValidateUnique where

import Control.Monad.State
import Data.Set as Set

import Latte.AbsLatte

import FrontEnd.Common

type UniqueState = Set Ident

run :: LatteProgram -> IO LatteProgram
run prog@(Program _ defs) = do
  evalStateT (Prelude.foldl (>>) (return ()) (Prelude.map validateUniqueDefinition defs)) initIdentState >> return prog

initIdentState :: UniqueState
initIdentState = Set.fromList [Ident "printInt", Ident "readInt", Ident "printString", Ident "readString", Ident "error"]

validateUniqueDefinition :: LatteTopDef -> StateT UniqueState IO ()
validateUniqueDefinition (FnDef loc _ ident args block) = do
  get >>= \st -> if Set.member ident st
    then lift $ fatalError loc (NameCollisionException ident)
    else modify $ Set.insert ident
  lift $ evalStateT (Prelude.foldl (>>) (return ()) (Prelude.map validateUniqueArg args)) initIdentState
  validateUniqueBlock block

validateUniqueBlock :: LatteBlock -> StateT UniqueState IO ()
validateUniqueBlock (Block _ stmts) = do
  lift $ evalStateT (Prelude.foldl (>>) (return ()) (Prelude.map validateUniqueStmt stmts)) initIdentState

validateUniqueArg :: LatteArg -> StateT UniqueState IO ()
validateUniqueArg (Arg loc _ ident) = do
  get >>= \st -> if Set.member ident st
    then lift $ fatalError loc (NameCollisionException ident)
    else modify $ Set.insert ident

validateUniqueStmt :: LatteStmt -> StateT UniqueState IO ()
validateUniqueStmt stmt = case stmt of
  (Decl _ _ items)  -> Prelude.foldl (>>) (return ()) $ Prelude.map validateUniqueItem items
  (BStmt _ block)   -> validateUniqueBlock block
  (Cond _ _ s)          -> validateUniqueStmt s
  (CondElse _ _ s1 s2)  -> validateUniqueStmt s1 >> validateUniqueStmt s2
  (While _ _ s)         -> validateUniqueStmt s
  _ -> return ()

validateUniqueItem :: LatteItem -> StateT UniqueState IO ()
validateUniqueItem item = do
  (loc, ident) <- case item of
    (NoInit loc ident) -> return (loc, ident)
    (Init loc ident _) -> return (loc, ident)
  get >>= \st -> if Set.member ident st
    then lift $ fatalError loc (NameCollisionException ident)
    else modify $ Set.insert ident