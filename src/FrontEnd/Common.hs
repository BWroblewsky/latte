module FrontEnd.Common where

import System.Exit
import System.IO

import Latte.AbsLatte

type LatteLoc = (Maybe (Int, Int))

type LatteProgram = Program LatteLoc
type LatteTopDef  = TopDef LatteLoc
type LatteBlock   = Block LatteLoc
type LatteStmt    = Stmt LatteLoc
type LatteExpr    = Expr LatteLoc
type LatteType    = Type LatteLoc
type LatteItem    = Item LatteLoc
type LatteArg     = Arg LatteLoc

type TypedProgram = Program (Type ())
type TypedTopDef  = TopDef (Type ())
type TypedBlock   = Block (Type ())
type TypedStmt    = Stmt (Type ())
type TypedExpr    = Expr (Type ())
type TypedType    = Type (Type ())
type TypedItem    = Item (Type ())
type TypedArg     = Arg (Type ())

data LatteException a = BNFCException String
                      | NoMainException
                      | MainTypeException (Type a)
                      | MainArgsException
                      | NameCollisionException Ident
                      | VarUndeclaredException Ident
                      | BadTypeException (Type a) (Type a)
                      | VarNotCallableException Ident
                      | VarNotIncrException Ident
                      | VoidReturnException (Type a)
                      | ValueReturnException
                      | NoReturnException Ident
                      | OperationUnknownException (Type a)
                      | NumberOfArgException Ident
                      | VoidArgException Ident
                      | VoidDeclException

showType :: Type a -> String
showType (Void _) = "void"
showType (Int _)  = "int"
showType (Str _)  = "string"
showType (Bool _) = "bool"
showType (Fun _ t _) = unwords [ "function returning type", showType t ]

getTypeA :: Type a -> a
getTypeA t = case t of
  (Void a) -> a
  (Int a)  -> a
  (Str a)  -> a
  (Bool a) -> a
  (Fun a _ _) -> a

getExprA :: Expr a -> a
getExprA expr = case expr of
  (EVar a _)    -> a
  (ELitInt a _) -> a
  (ELitTrue a)  -> a
  (ELitFalse a) -> a

  (EApp a _ _)  -> a
  (EString a _) -> a

  (Neg a _) -> a
  (Not a _) -> a

  (EMul a _ _ _) -> a
  (EAdd a _ _ _) -> a
  (ERel a _ _ _) -> a
  (EAnd a _ _)  -> a
  (EOr a _ _)   -> a

instance Show (LatteException a) where
  show exception = case exception of
    (BNFCException msg)     -> unwords [ "BNFC error has occured:", msg ]
    (NoMainException)       -> "Main function is not present in the program"
    (MainTypeException t)   -> unwords [ "Main function must return type int, but it returns type", showType t ]
    (MainArgsException)     -> "Main function cannot take any arguments"
    (NameCollisionException (Ident name)) -> unwords [ "Identifier", name, "is already declared in this scope" ]
    (VarUndeclaredException (Ident name)) -> unwords [ "Variable", name, "is used before declaration" ]
    (BadTypeException t1 t2) -> unwords [ "Expresion should have type", showType t1, "but it has type", showType t2 ]
    (VarNotCallableException (Ident name)) -> unwords [ "Variable", name, "is not callable" ]
    (VarNotIncrException (Ident name))  -> unwords [ "Variable", name, "is not incrementable/decrementable" ]
    (VoidReturnException t)             -> unwords [ "Function has to return", showType t ]
    (ValueReturnException)              -> "Function returns void, it cannot return a value"
    (NoReturnException (Ident name))    -> unwords [ "Function", name, "has runs without return statement" ]
    (OperationUnknownException t)       -> unwords [ "Operation is not specified for type", showType t ]
    (NumberOfArgException (Ident name)) -> unwords [ "Function", name, "takes different number of arguments" ]
    (VoidArgException (Ident name))     -> unwords [ "Argument", name, "cannot have type void" ]
    (VoidDeclException)                 -> "Cannot declare a void variable"

showLocation :: LatteLoc -> String
showLocation (Just (row, col))  = unwords [ "At line", show row, ", column", show col]
showLocation Nothing            = "At location unknown"

fatalError :: LatteLoc -> LatteException a -> IO b
fatalError loc exception = do
  ((hPutStrLn stderr). unlines) [ "ERROR", showLocation loc, show exception ]
  exitFailure
