module FrontEnd.ReduceConstants where

import Latte.AbsLatte

import FrontEnd.Common

run :: TypedProgram -> IO TypedProgram
run (Program _t definitions) = return (Program _t (map reduceConstantsDefinition definitions))

reduceConstantsDefinition :: TypedTopDef -> TypedTopDef
reduceConstantsDefinition (FnDef _t t ident args block) = FnDef _t t ident args (reduceConstantsBlock block)

reduceConstantsBlock :: TypedBlock -> TypedBlock
reduceConstantsBlock (Block _t stmts) = Block _t (map reduceConstantsStmt stmts)

reduceConstantsStmt :: TypedStmt -> TypedStmt
reduceConstantsStmt stmt = case stmt of
  (BStmt _t block)         -> BStmt _t (reduceConstantsBlock block)
  (Ass _t ident expr)      -> Ass _t ident (reduceConstantsExpr expr)
  (Ret _t expr)            -> Ret _t (reduceConstantsExpr expr)
  (Cond _t expr s1)        -> Cond _t (reduceConstantsExpr expr) (reduceConstantsStmt s1)
  (CondElse _t expr s1 s2) -> CondElse _t (reduceConstantsExpr expr) (reduceConstantsStmt s1) (reduceConstantsStmt s2)
  (While _t expr s1)       -> While _t (reduceConstantsExpr expr) (reduceConstantsStmt s1)
  (SExp _t expr)           -> SExp _t (reduceConstantsExpr expr)
  (Decl _t t items)        -> Decl _t t (map reduceConstantsItem items)
  _                         -> stmt

reduceConstantsItem :: TypedItem -> TypedItem
reduceConstantsItem item = case item of
  (NoInit _t ident)    -> NoInit _t ident
  (Init _t ident expr) -> Init _t ident (reduceConstantsExpr expr)

reduceConstantsExpr :: TypedExpr -> TypedExpr
reduceConstantsExpr expr = case expr of
  (EApp _t ident exprs) -> EApp _t ident (map reduceConstantsExpr exprs)
  (Neg _t e) -> case reduceConstantsExpr e of
    (ELitInt _ i) -> ELitInt _t (-i)
    e' -> (Neg _t e')
  (Not _t e) -> case reduceConstantsExpr e of
    (ELitTrue _)  -> ELitFalse _t
    (ELitFalse _) -> ELitTrue _t
    e' -> (Not _t e')
  (EMul _t e1 op e2) -> case (reduceConstantsExpr e1, reduceConstantsExpr e2, op) of
    (ELitInt _ i1, ELitInt _ i2, Times _) -> ELitInt _t $ i1 * i2
    (ELitInt _ i1, ELitInt _ i2, Div _)   -> ELitInt _t $ i1 `div` i2
    (ELitInt _ i1, ELitInt _ i2, Mod _)   -> ELitInt _t $ i1 `mod` i2
    (e1', e2', op) -> EMul _t e1' op e2'
  (EAdd _t e1 op e2) -> case (reduceConstantsExpr e1, reduceConstantsExpr e2, op) of
    (ELitInt _ i1, ELitInt _ i2, Plus _)  -> ELitInt _t $ i1 + i2
    (EString _ s1, EString _ s2, Plus _)  -> EString _t $ s1 ++ s2
    (ELitInt _ i1, ELitInt _ i2, Minus _) -> ELitInt _t $ i1 - i2
    (e1', e2', op) -> EAdd _t e1' op e2'
  (ERel _t e1 op e2) -> case (reduceConstantsExpr e1, reduceConstantsExpr e2, op) of
    (ELitInt _ i1, ELitInt _ i2, LTH _)   -> if i1 < i2  then ELitTrue _t else ELitFalse _t
    (ELitInt _ i1, ELitInt _ i2, LE _)    -> if i1 <= i2 then ELitTrue _t else ELitFalse _t
    (ELitInt _ i1, ELitInt _ i2, GTH _)   -> if i1 > i2  then ELitTrue _t else ELitFalse _t
    (ELitInt _ i1, ELitInt _ i2, GE _)    -> if i1 >= i2 then ELitTrue _t else ELitFalse _t
    (ELitInt _ i1, ELitInt _ i2, EQU _)   -> if i1 == i2 then ELitTrue _t else ELitFalse _t
    (ELitInt _ i1, ELitInt _ i2, NE _)    -> if i1 /= i2 then ELitTrue _t else ELitFalse _t
    (ELitFalse _, ELitFalse _, EQU _) -> ELitTrue _t
    (ELitTrue _, ELitTrue _, EQU _)   -> ELitTrue _t
    (ELitTrue _, ELitFalse _, EQU _)  -> ELitFalse _t
    (ELitFalse _, ELitTrue _, EQU _)  -> ELitFalse _t
    (ELitFalse _, ELitFalse _, NE _)  -> ELitFalse _t
    (ELitTrue _, ELitTrue _, NE _)    -> ELitFalse _t
    (ELitTrue _, ELitFalse _, NE _)   -> ELitTrue _t
    (ELitFalse _, ELitTrue _, NE _)   -> ELitTrue _t
    (e1', e2', op) -> ERel _t e1' op e2'
  (EAnd _t e1 e2) -> case (reduceConstantsExpr e1, reduceConstantsExpr e2) of
    (ELitFalse _, _)         -> ELitFalse _t
    (ELitTrue _, ELitTrue _) -> ELitTrue _t
    (e1', e2')   -> EAnd _t e1' e2'
  (EOr _t e1 e2) -> case (reduceConstantsExpr e1, reduceConstantsExpr e2) of
    (ELitTrue _, _)             -> ELitTrue _t
    (ELitFalse _, ELitFalse _)  -> ELitFalse _t
    (e1', e2')   -> EOr _t e1' e2'
  _ -> expr