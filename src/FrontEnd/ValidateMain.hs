module FrontEnd.ValidateMain where

import Latte.AbsLatte

import FrontEnd.Common

run :: LatteProgram -> IO LatteProgram
run prog@(Program _ defs) = findMain defs >>= checkMain >> return prog

findMain :: [LatteTopDef] -> IO LatteTopDef
findMain defs = do
  let mains = filter (\(FnDef _ _ (Ident name) _ _) -> name == "main") defs
  if mains == [] then fatalError Nothing NoMainException else return $ head mains

checkMain :: LatteTopDef -> IO ()
checkMain (FnDef loc t _ args _) = do
  case t of {(Int _) -> return (); _ -> fatalError loc (MainTypeException t)}
  case args of {[] -> return (); _ -> fatalError loc (MainArgsException)}