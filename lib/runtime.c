#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void printString(char *str) {
    puts(str);
}

char * readString() {
    char * buffer = NULL;
    size_t buf_size;

    getline(&buffer, &buf_size, stdin);

    size_t len = strlen(buffer);

    if(buffer[len - 1] == '\n')
        buffer[len - 1] = 0;

    return buffer;
}

char * concat(char * str1, char * str2) {
    size_t len = strlen(str1) + strlen(str2) + 1;
    char * buffer = malloc(len);
    strcpy(buffer, str1);
    strcat(buffer, str2);
    return buffer;
}

void printInt(int x) {
    printf("%d\n", x);
}

int readInt() {
    int x;
    scanf("%d", &x);
    free(readString());
    return x;
}

void error() {
    fputs("runtime error\n", stderr);
    exit(1);
}
