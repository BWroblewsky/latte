declare i32 @readInt()
declare void @printInt(i32)


define i32 @main() {
L0:
	br label %L1
L1:
	br i1 1, label %L2, label %L3
L2:
	%t4 = call i32 @readInt()
	%t5 = shl i32 %t4, 3
	call void @printInt(i32 %t5)
	%t7 = shl i32 %t4, 11
	call void @printInt(i32 %t7)
	%t9 = ashr i32 %t4, 3
	call void @printInt(i32 %t9)
	br label %L1
L3:
	ret i32 0
}
