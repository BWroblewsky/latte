declare void @printString(i8*)


@s3 = internal constant [4 x i8] c"\66\6F\6F\00"

define void @__foo() {
L2:
	%t4 = bitcast [4 x i8]* @s3 to i8*
	call void @printString(i8* %t4)
	ret void
}


define i32 @main() {
L0:
	call void @__foo()
	ret i32 0
}
