declare void @printInt(i32)


define i32 @main() {
L0:
	call void @printInt(i32 1)
	br label %L2
L2:
	%t5 = phi i32 [1, %L0], [%t11, %L3]
	%t6 = phi i32 [1, %L0], [%t10, %L3]
	%t8 = icmp slt i32 %t6, 5000000
	br i1 %t8, label %L3, label %L4
L3:
	call void @printInt(i32 %t6)
	%t10 = add i32 %t5, %t6
	%t11 = sub i32 %t10, %t5
	br label %L2
L4:
	ret i32 0
}
