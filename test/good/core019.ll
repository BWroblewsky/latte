declare void @printInt(i32)
declare void @printString(i8*)


@s19 = internal constant [4 x i8] c"\66\6F\6F\00"

define i32 @main() {
L0:
	call void @printInt(i32 1)
	call void @printInt(i32 78)
	br label %L3
L3:
	%t6 = phi i32 [78, %L0], [%t9, %L4]
	%t8 = icmp sgt i32 %t6, 76
	br i1 %t8, label %L4, label %L5
L4:
	%t9 = sub i32 %t6, 1
	call void @printInt(i32 %t9)
	%t11 = add i32 %t9, 7
	call void @printInt(i32 %t11)
	br label %L3
L5:
	call void @printInt(i32 %t6)
	%t14 = icmp sgt i32 %t6, 4
	br i1 %t14, label %L15, label %L16
L15:
	call void @printInt(i32 4)
	br label %L17
L16:
	%t20 = bitcast [4 x i8]* @s19 to i8*
	call void @printString(i8* %t20)
	br label %L17
L17:
	call void @printInt(i32 %t6)
	ret i32 0
}
