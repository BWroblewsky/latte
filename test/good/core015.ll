declare void @printInt(i32)


define i32 @__ev(i32 %t3) {
L4:
	%t5 = icmp sgt i32 %t3, 0
	br i1 %t5, label %L6, label %L7
L6:
	%t9 = sub i32 %t3, 2
	%t10 = call i32 @__ev(i32 %t9)
	ret i32 %t10
L7:
	%t11 = icmp slt i32 %t3, 0
	br i1 %t11, label %L12, label %L13
L12:
	ret i32 0
L13:
	ret i32 1
}


define i32 @main() {
L0:
	%t1 = call i32 @__ev(i32 17)
	call void @printInt(i32 %t1)
	ret i32 0
}
