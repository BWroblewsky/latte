declare void @printInt(i32)


define void @__p() {
L3:
	ret void
}


define i32 @main() {
L0:
	call void @__p()
	call void @printInt(i32 1)
	ret i32 0
}
