

define i32 @__f() {
L0:
	ret i32 0
}


define i32 @__g() {
L1:
	ret i32 0
}


define void @__p() {
L2:
	ret void
}


define i32 @main() {
L3:
	call void @__p()
	ret i32 0
}
