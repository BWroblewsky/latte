declare void @printInt(i32)


define i32 @main() {
L0:
	br label %L1
L1:
	%t4 = phi i32 [17, %L0], [%t6, %L2]
	%t5 = icmp sgt i32 %t4, 0
	br i1 %t5, label %L2, label %L3
L2:
	%t6 = sub i32 %t4, 2
	br label %L1
L3:
	%t7 = icmp slt i32 %t4, 0
	br i1 %t7, label %L8, label %L9
L8:
	call void @printInt(i32 0)
	ret i32 0
L9:
	call void @printInt(i32 1)
	ret i32 0
}
