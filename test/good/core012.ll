declare void @printInt(i32)
declare void @printString(i8*)


@s19 = internal constant [6 x i8] c"\66\61\6C\73\65\00"

@s8 = internal constant [21 x i8] c"\73\74\72\69\6E\67\20\63\6F\6E\63\61\74\65\6E\61\74\69\6F\6E\00"

@s16 = internal constant [5 x i8] c"\74\72\75\65\00"

define void @__printBool(i1 %t11) {
L12:
	br i1 %t11, label %L13, label %L14
L13:
	%t17 = bitcast [5 x i8]* @s16 to i8*
	call void @printString(i8* %t17)
	ret void
L14:
	%t20 = bitcast [6 x i8]* @s19 to i8*
	call void @printString(i8* %t20)
	ret void
}


define i32 @main() {
L0:
	call void @printInt(i32 33)
	call void @printInt(i32 79)
	call void @printInt(i32 -1288)
	call void @printInt(i32 22)
	call void @printInt(i32 0)
	call void @__printBool(i1 1)
	call void @__printBool(i1 0)
	%t9 = bitcast [21 x i8]* @s8 to i8*
	call void @printString(i8* %t9)
	ret i32 0
}
