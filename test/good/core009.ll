declare void @printInt(i32)


define i32 @__foo() {
L3:
	ret i32 10
}


define i32 @main() {
L0:
	%t1 = call i32 @__foo()
	call void @printInt(i32 %t1)
	ret i32 0
}
