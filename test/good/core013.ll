declare void @printInt(i32)
declare void @printString(i8*)


@s55 = internal constant [2 x i8] c"\21\00"

@s1 = internal constant [3 x i8] c"\26\26\00"

@s66 = internal constant [6 x i8] c"\66\61\6C\73\65\00"

@s69 = internal constant [5 x i8] c"\74\72\75\65\00"

@s28 = internal constant [3 x i8] c"\7C\7C\00"

define void @__printBool(i1 %t60) {
L61:
	%t62 = xor i1 1, %t60
	br i1 %t62, label %L63, label %L64
L63:
	%t67 = bitcast [6 x i8]* @s66 to i8*
	call void @printString(i8* %t67)
	br label %L65
L64:
	%t70 = bitcast [5 x i8]* @s69 to i8*
	call void @printString(i8* %t70)
	br label %L65
L65:
	ret void
}


define i1 @__test(i32 %t72) {
L73:
	call void @printInt(i32 %t72)
	%t75 = icmp sgt i32 %t72, 0
	ret i1 %t75
}


define i32 @main() {
L0:
	%t2 = bitcast [3 x i8]* @s1 to i8*
	call void @printString(i8* %t2)
	%t4 = call i1 @__test(i32 -1)
	br i1 %t4, label %L5, label %L6
L5:
	%t7 = call i1 @__test(i32 0)
	br label %L6
L6:
	%t8 = phi i1 [0, %L0], [%t7, %L5]
	call void @__printBool(i1 %t8)
	%t10 = call i1 @__test(i32 -2)
	br i1 %t10, label %L11, label %L12
L11:
	%t13 = call i1 @__test(i32 1)
	br label %L12
L12:
	%t14 = phi i1 [0, %L6], [%t13, %L11]
	call void @__printBool(i1 %t14)
	%t16 = call i1 @__test(i32 3)
	br i1 %t16, label %L17, label %L18
L17:
	%t19 = call i1 @__test(i32 -5)
	br label %L18
L18:
	%t20 = phi i1 [0, %L12], [%t19, %L17]
	call void @__printBool(i1 %t20)
	%t22 = call i1 @__test(i32 234234)
	br i1 %t22, label %L23, label %L24
L23:
	%t25 = call i1 @__test(i32 21321)
	br label %L24
L24:
	%t26 = phi i1 [0, %L18], [%t25, %L23]
	call void @__printBool(i1 %t26)
	%t29 = bitcast [3 x i8]* @s28 to i8*
	call void @printString(i8* %t29)
	%t31 = call i1 @__test(i32 -1)
	br i1 %t31, label %L33, label %L32
L32:
	%t34 = call i1 @__test(i32 0)
	br label %L33
L33:
	%t35 = phi i1 [1, %L24], [%t34, %L32]
	call void @__printBool(i1 %t35)
	%t37 = call i1 @__test(i32 -2)
	br i1 %t37, label %L39, label %L38
L38:
	%t40 = call i1 @__test(i32 1)
	br label %L39
L39:
	%t41 = phi i1 [1, %L33], [%t40, %L38]
	call void @__printBool(i1 %t41)
	%t43 = call i1 @__test(i32 3)
	br i1 %t43, label %L45, label %L44
L44:
	%t46 = call i1 @__test(i32 -5)
	br label %L45
L45:
	%t47 = phi i1 [1, %L39], [%t46, %L44]
	call void @__printBool(i1 %t47)
	%t49 = call i1 @__test(i32 234234)
	br i1 %t49, label %L51, label %L50
L50:
	%t52 = call i1 @__test(i32 21321)
	br label %L51
L51:
	%t53 = phi i1 [1, %L45], [%t52, %L50]
	call void @__printBool(i1 %t53)
	%t56 = bitcast [2 x i8]* @s55 to i8*
	call void @printString(i8* %t56)
	call void @__printBool(i1 1)
	call void @__printBool(i1 0)
	ret i32 0
}
