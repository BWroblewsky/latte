declare void @printInt(i32)


define i32 @main() {
L0:
	call void @printInt(i32 45)
	call void @printInt(i32 -36)
	ret i32 0
}
