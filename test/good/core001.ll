declare void @printInt(i32)
declare void @printString(i8*)
declare i8* @concat(i8*, i8*)


@s9 = internal constant [1 x i8] c"\00"

@s28 = internal constant [9 x i8] c"\2F\2A\20\77\6F\72\6C\64\00"

@s21 = internal constant [2 x i8] c"\3D\00"

@s25 = internal constant [9 x i8] c"\68\65\6C\6C\6F\20\2A\2F\00"

define i32 @__fac(i32 %t31) {
L32:
	br label %L33
L33:
	%t37 = phi i32 [1, %L32], [%t40, %L34]
	%t38 = phi i32 [%t31, %L32], [%t41, %L34]
	%t39 = icmp sgt i32 %t38, 0
	br i1 %t39, label %L34, label %L35
L34:
	%t40 = mul i32 %t37, %t38
	%t41 = sub i32 %t38, 1
	br label %L33
L35:
	ret i32 %t37
}


define i32 @__ifac(i32 %t69) {
L70:
	%t71 = call i32 @__ifac2f(i32 1, i32 %t69)
	ret i32 %t71
}


define i32 @__ifac2f(i32 %t72, i32 %t73) {
L74:
	%t75 = icmp eq i32 %t72, %t73
	br i1 %t75, label %L76, label %L77
L76:
	ret i32 %t72
L77:
	%t78 = icmp sgt i32 %t72, %t73
	br i1 %t78, label %L79, label %L80
L79:
	ret i32 1
L80:
	%t81 = add i32 %t72, %t73
	%t82 = ashr i32 %t81, 1
	%t83 = call i32 @__ifac2f(i32 %t72, i32 %t82)
	%t84 = add i32 %t82, 1
	%t85 = call i32 @__ifac2f(i32 %t84, i32 %t73)
	%t86 = mul i32 %t83, %t85
	ret i32 %t86
}


define i32 @__mfac(i32 %t51) {
L52:
	%t53 = icmp eq i32 %t51, 0
	br i1 %t53, label %L54, label %L55
L54:
	ret i32 1
L55:
	%t57 = sub i32 %t51, 1
	%t58 = call i32 @__nfac(i32 %t57)
	%t59 = mul i32 %t51, %t58
	ret i32 %t59
}


define i32 @__nfac(i32 %t60) {
L61:
	%t62 = icmp ne i32 %t60, 0
	br i1 %t62, label %L63, label %L64
L63:
	%t66 = sub i32 %t60, 1
	%t67 = call i32 @__mfac(i32 %t66)
	%t68 = mul i32 %t67, %t60
	ret i32 %t68
L64:
	ret i32 1
}


define i8* @__repStr(i8* %t87, i32 %t88) {
L89:
	%t90 = bitcast [1 x i8]* @s9 to i8*
	br label %L91
L91:
	%t96 = phi i8* [%t90, %L89], [%t99, %L92]
	%t97 = phi i32 [0, %L89], [%t100, %L92]
	%t98 = icmp slt i32 %t97, %t88
	br i1 %t98, label %L92, label %L93
L92:
	%t99 = call i8* @concat(i8* %t96, i8* %t87)
	%t100 = add i32 %t97, 1
	br label %L91
L93:
	ret i8* %t96
}


define i32 @__rfac(i32 %t42) {
L43:
	%t44 = icmp eq i32 %t42, 0
	br i1 %t44, label %L45, label %L46
L45:
	ret i32 1
L46:
	%t48 = sub i32 %t42, 1
	%t49 = call i32 @__rfac(i32 %t48)
	%t50 = mul i32 %t42, %t49
	ret i32 %t50
}


define i32 @main() {
L0:
	%t1 = call i32 @__fac(i32 10)
	call void @printInt(i32 %t1)
	%t3 = call i32 @__rfac(i32 10)
	call void @printInt(i32 %t3)
	%t5 = call i32 @__mfac(i32 10)
	call void @printInt(i32 %t5)
	%t7 = call i32 @__ifac(i32 10)
	call void @printInt(i32 %t7)
	br label %L11
L11:
	%t15 = phi i32 [10, %L0], [%t19, %L12]
	%t16 = phi i32 [1, %L0], [%t18, %L12]
	%t17 = icmp sgt i32 %t15, 0
	br i1 %t17, label %L12, label %L13
L12:
	%t18 = mul i32 %t16, %t15
	%t19 = sub i32 %t15, 1
	br label %L11
L13:
	call void @printInt(i32 %t16)
	%t22 = bitcast [2 x i8]* @s21 to i8*
	%t23 = call i8* @__repStr(i8* %t22, i32 60)
	call void @printString(i8* %t23)
	%t26 = bitcast [9 x i8]* @s25 to i8*
	call void @printString(i8* %t26)
	%t29 = bitcast [9 x i8]* @s28 to i8*
	call void @printString(i8* %t29)
	ret i32 0
}
