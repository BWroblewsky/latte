declare void @printInt(i32)


define i32 @__fac(i32 %t3) {
L4:
	br label %L5
L5:
	%t9 = phi i32 [1, %L4], [%t12, %L6]
	%t10 = phi i32 [%t3, %L4], [%t13, %L6]
	%t11 = icmp sgt i32 %t10, 0
	br i1 %t11, label %L6, label %L7
L6:
	%t12 = mul i32 %t9, %t10
	%t13 = sub i32 %t10, 1
	br label %L5
L7:
	ret i32 %t9
}


define i32 @main() {
L0:
	%t1 = call i32 @__fac(i32 5)
	call void @printInt(i32 %t1)
	ret i32 0
}
