declare void @printInt(i32)
declare void @printString(i8*)


@s24 = internal constant [6 x i8] c"\66\61\6C\73\65\00"

@s21 = internal constant [5 x i8] c"\74\72\75\65\00"

define i1 @__dontCallMe(i32 %t13) {
L14:
	call void @printInt(i32 %t13)
	ret i1 1
}


define i1 @__implies(i1 %t27, i1 %t28) {
L29:
	%t30 = xor i1 1, %t27
	br i1 %t30, label %L32, label %L31
L31:
	%t33 = icmp eq i1 %t27, %t28
	br label %L32
L32:
	%t34 = phi i1 [1, %L29], [%t33, %L31]
	ret i1 %t34
}


define void @__printBool(i1 %t16) {
L17:
	br i1 %t16, label %L18, label %L19
L18:
	%t22 = bitcast [5 x i8]* @s21 to i8*
	call void @printString(i8* %t22)
	br label %L20
L19:
	%t25 = bitcast [6 x i8]* @s24 to i8*
	call void @printString(i8* %t25)
	br label %L20
L20:
	ret void
}


define i32 @main() {
L0:
	call void @__printBool(i1 1)
	call void @__printBool(i1 1)
	call void @__printBool(i1 0)
	call void @__printBool(i1 1)
	%t5 = call i1 @__implies(i1 0, i1 0)
	call void @__printBool(i1 %t5)
	%t7 = call i1 @__implies(i1 0, i1 1)
	call void @__printBool(i1 %t7)
	%t9 = call i1 @__implies(i1 1, i1 0)
	call void @__printBool(i1 %t9)
	%t11 = call i1 @__implies(i1 1, i1 1)
	call void @__printBool(i1 %t11)
	ret i32 0
}
