declare i32 @readInt()
declare void @printInt(i32)
declare i8* @readString()
declare void @printString(i8*)
declare i8* @concat(i8*, i8*)


define i32 @main() {
L0:
	%t1 = call i32 @readInt()
	%t2 = call i8* @readString()
	%t3 = call i8* @readString()
	%t4 = sub i32 %t1, 5
	call void @printInt(i32 %t4)
	%t6 = call i8* @concat(i8* %t2, i8* %t3)
	call void @printString(i8* %t6)
	ret i32 0
}
